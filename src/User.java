import java.sql.ResultSet; 
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 * User object class
 * Stores information about users
 * @version 1.0
 */
public class User {
    
    private String username;
    private String password;
    private long cardDetails;
    
    /**
    * Default constructor
    */
    User(){}
    
    /**
     * Constructor for user object
     * @param username The name of the user
     * @param password The user's password
     * @param cardDetails The user's card details
     */
    User( String username, String password, long cardDetails ){
        this.username = username;
        this.password = password;
        this.cardDetails = cardDetails;
    }
    
    // Getters for this class:
    
    /**
     * @return The username
     */
    public String getUsername(){
        return this.username;
    }
    
    /**
     * @return The User's password
     */
    public String getPassword(){
        return this.password;
    }
    
    /**
     * @return The User's card details
     */
    public long getCardDetails(){
        return this.cardDetails;
    }
    
    // Setters for this class:
    
    public void setUsername( String username ){
        this.username = username;
       
    }
    
    public void setPassword( String password ){  
        this.password = password;
        
    }
    
    public void setCardDetails( Long cardDetails ){        
        this.cardDetails = cardDetails;
    }
    
    // Database functionality for this class:
    
    /**
     * Loads the User class from the database for a given username
     * @param userName The username
     */
    public void retrieve( String userName ){

        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select username, userpassword, usercarddetails from users where username = '" + userName + "'" ); 

            while( result.next() ){ 
                username = result.getString( "USERNAME" ); 
                password = result.getString( "USERPASSWORD" ); 
                cardDetails = result.getLong( "USERCARDDETAILS" ); 
            } //while( result.next() ){ 
        } catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error retrieving user" );
        } 

    } 
    
    /**
     *Method to retrieve a users card details and charge
     *them for a booking
     *@param userName The username
     */
      public void chargeCard( String userName ){
        try{
            Long userCardNumber = Long.parseLong( DBConnection.getQuery( "select usercarddetails from users where username = '" + userName + "'", DBConnection.connection ) );
        }catch( Exception e ){   
            JOptionPane.showMessageDialog( null, "Error retrieving user details" );
        }
        JOptionPane.showMessageDialog( null, "The card has been charged" );
    }

    /**
     *Saves the User to the database
     *@param currentName The username of user to update
     */
    public void save( String currentName){
        try{
            DBConnection.addQuery( "update users set username = '" + username + "', userpassword = '" + password + "', usercarddetails = " + cardDetails + " where username = '" + currentName + "'", DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error with updating database" );
        } 

    }
    
    /**
     *Adds new user to the database using current objects data
     */
    public void addNewMember(){
        try{
            DBConnection.addQuery( "insert into users values ('" + username + "', '" + password + "', " + cardDetails + ")" , DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error with updating database" );
        }
    }
}
