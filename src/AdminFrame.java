
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/*
 * This is the AdminFrame class used to create a GUI panel for the  
 * administrator to interact with for various system operations
 * 
 * @author: Niral Patel
 * @version: v1.0
 * 
 */
public class AdminFrame extends JFrame{
    /*
     * Various image files used for the buttons, instead of 
     * standard swing buttons.
     */
    ImageIcon image = new ImageIcon( "change account details.jpg" );
    ImageIcon image1 = new ImageIcon( "change film details.jpg" );
    ImageIcon image2 = new ImageIcon( "newsletter.jpg" );
    ImageIcon image3 = new ImageIcon( "exit.jpg" );
    ImageIcon image4 = new ImageIcon( "log out.jpg" );
    JButton exit = new JButton( image3 );
    JButton bAccountDetails = new JButton( image );
    JButton bFilmDetails = new JButton( image1 );
    JButton bNewsletter = new JButton( image2 );
    JButton blogOut = new JButton( image4 );
    
    
    AdminFrame( String adminName ){
        //Setting up the frame and the positioning of the various buttons
        super( "Admin Home" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        exit.setBounds(700,10,100,20);
        bAccountDetails.setBounds(100,100,200,200);
        bFilmDetails.setBounds(350,100,200,200);
        blogOut.setBounds(550,10,100,20);
        bNewsletter.setBounds(600,100,200,200);
        
        bGPanel.add( exit );
        bGPanel.add(bAccountDetails);
        bGPanel.add(bFilmDetails);
        bGPanel.add(blogOut);
        bGPanel.add(bNewsletter);
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setSize(950,400);
        setLocationRelativeTo(null);
        
        //Various methods for when buttons are pressed, to create
        //new frames, or to access back-end class methods to 
        //retrieve information from the database
        clickAccountDetails();
        clickFilmDetails();
        clickLogout();
        clickNewsletter();
        clickExit();
    }
    
    /*
     * clickAccountDetails method
     * 
     * when the bAccountDetails button is clicked it 
     * creates a new [@link EditMemberFrame] for the 
     * Admin to interact with
     * 
     */
    public void clickAccountDetails(){
        bAccountDetails.addActionListener( new ActionListener() {
                @Override
                public void actionPerformed( ActionEvent ae ){   
                    EditMemberFrame temp = new EditMemberFrame();
                }
         });
    }
    
    /*
     * clickFilmDetails method
     * 
     * when the bFilmDetails button is clicked it 
     * creates a new [@link EditFilmFrame] for the 
     * Admin to interact with
     * 
     */
    public void clickFilmDetails(){
        bFilmDetails.addActionListener( new ActionListener(){
                @Override
                public void actionPerformed( ActionEvent ae ){  
                    EditFilmFrame temp = new EditFilmFrame();
                }
        });
    }
    
    /*
     * clickLogout method
     * 
     * when the blogOut button is clicked returnds
     * Admin to the home screen [@link ImageInJFrame].+
     * 
     */
    public void clickLogout(){
        blogOut.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                Login.loggedIn = false;
                Login.loggedInUserName = "";
                ImageInJFrame.mainFrame.setVisible(true);
                dispose();
                }
        });
    }
    
    /*
     * clickNewletter method
     * 
     * when the bNewsletter button is clicked it 
     * creates a new [@link NewsletterFrame] for the 
     * Admin to interact with
     */
    public void clickNewsletter(){
        bNewsletter.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){  
                NewsletterFrame frame = new NewsletterFrame();
            }
        });
    }
    
    /*
     * clickExit method
     * 
     * when the exit button is clicked it exits 
     * the system.
     * 
     */
    public void clickExit(){
        exit.addActionListener( new ActionListener(){
            @Override
            public void actionPerformed( ActionEvent ae ){
                System.exit(0);
            }
        });
    } 
}
