
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Checks class used for checking the validity
 * of new passwords
 * 
 * @author Niral Patel
 * @version v1.0
 * 
 */

public class Checks {
    /**
     * This method checks user input to make it is correct 
     * and then returns the input
     * 
     * @param   regex       The regex to be used for the input
     * @param   password    The password to test
     * @return              The user input
     * 
     */
    public static boolean userInputCheck( String password, String regex ) throws Exception{
           boolean valid = false;
        
           Pattern patternObj = Pattern.compile( regex );
        
           Matcher matcherObj = patternObj.matcher(password);
           
           if ( matcherObj.find()){
              //check that input conforms to regex 
              boolean correct = matcherObj.matches();
              if( correct == true ){
                  valid = true;
              }
           }else{
                throw new Exception();
           }
           return valid;
        }
    }

