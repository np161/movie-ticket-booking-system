
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * WriteReviewFrame Class
 * Frame for creating new film reviews.
 * @Author: Niral Patel
 * @version 1.0
 */
class WriteReviewFrame extends JFrame{
    // Set up Swing components
    ImageIcon image1 = new ImageIcon( "save review.jpg" );
    JTextArea reviewContents;
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    JButton save = new JButton( image1 );
    String filmname;
    
    /**
    * Constructor
    * @param filmname The name of the film
    */
    WriteReviewFrame( String filmName ){
    
        // Set up swing panel
        super( "Write Review" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);

        this.filmname = filmName;
        
        reviewContents = new JTextArea( "Please Enter review here" );
        reviewContents.setBounds(37,50,650,300);
        reviewContents.setLineWrap(true);
        JScrollPane scroller = new JScrollPane(reviewContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.setBounds(37,50,650,300);
        save.setBounds(250,370,200,20);

        bGPanel.add( save ); //Add write review button to panel
        bGPanel.add( reviewContents ); //Add review textArea to panel
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(750,450);
        setLocationRelativeTo(null);
        setVisible(true);
        clickSave();
        clickBack();
    }
    
    /**
    * When the save review button is clicked it saves
    * the review in the database as long as the review 
    * has not been written before
    */
    @Override
    public void clickSave(){
        save.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    // Find text and create Review
                    String newText = reviewContents.getText();
                    Review review = new Review();
                    review.setFilmName(filmname);
                    review.setReviewText(newText);
                    review.setUserCreator(Login.loggedInUserName);
                    // Check review has not been written before
                    if( review.checkWritten() == true ){
                        JOptionPane.showMessageDialog( null, "You have already written a review for this film" );
                    } else {
                        review.add();
                        JOptionPane.showMessageDialog( null, "The review has been saved" );
                        dispose();
                    }
                }
        });
    }
    
    /**
    * Back button
    */
    @Override
    public void clickBack(){
        // Go back
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }
}
