/**
 * Login Class
 * Keeps track of user logged in
 * @version 1.0
 * @author Niral Patel
 */
public class Login {

    static boolean loggedIn = false; 
    static String loggedInUserName = ""; 

    /**
     * Checks username and password patch and sets user as logged in
     * @param username The username of the user
     * @param password The user's password
     * @return Whether the user has logged in successfully
     */
    public static boolean login( String username, String password ){ 

        boolean successfulLogin = false; 

        if( DBConnection.checkUserDetails( username, password ) == true ){
            successfulLogin = true; 
            loggedIn = true; 
            loggedInUserName = username; 
        }
        return successfulLogin; 
    }
        
     /**
     * Checks username and password patch and sets admin as logged in
     * @param username The username of the administrator
     * @param password The admin's password
     * @return Whether the admin has logged in successfully
     */
    public static boolean adminLogin( String username, String password ){ 

        boolean successfulLogin = false; 

        if( DBConnection.checkAdminUserDetails( username, password ) == true ){
            successfulLogin = true; 
            loggedIn = true; 
            loggedInUserName = username; 
        }
        return successfulLogin; 
    }
}
