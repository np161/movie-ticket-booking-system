
import javax.swing.JOptionPane;

/**
 * Newsletter class
 * Holds information about newsletters
 * @Author: Niral Patel
 * @version 1.0
 */
public class Newsletter {
    
    private String text;
    
    /**
    * Default constructor
    */
    Newsletter(){}
    
    /**
     * Constructor
     * @param text The text for the Newsletter
     */
    Newsletter( String text ){
        this.text = text;
    }
    
    // Getters for this class:
    
    /**
     * @return The text for this Newsletter
     */
    public String getText(){
        return this.text;
    }
    
    // Setters for this class:
    
    /**
     * Sets the text for the Newsletter
     * @param text The text
     */
    public void setText( String text ){
        this.text = text;
    }
    
    // Database functionality
    
    /**
     * Retrieve newsletter contents from database
     */
    public void retrieve(){
        try{
            text = DBConnection.getQuery("select newstext from newsletter", DBConnection.connection); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error retireving newsletter" );
        } 
    } 

    /**
     * Saves newsletter content to database
     */
    public void save() throws Exception{
        try{
            DBConnection.addQuery( "update newsletter set newstext = '" + text + "'", DBConnection.connection ); 
        }catch ( Exception e ){ 
            throw new Exception();
        } 
    }  
}