import java.sql.ResultSet; 
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 * AdminUser class stores information about Admin users
 * 
 * @Author: Niral Patel
 * @version 1.0
 */

public class AdminUser {
    private String name; private String password; 

    AdminUser(){} 
 
    /**
     * Constructor
     * @param name The Admin name 
     * @param password The Admin password
     */
 
    AdminUser( String name, String password ){
        this.name = name; 
        this.password = password; 
    } 

    // Getters 
    
    /**
     * @return The AdminUser's name
     */
 
    public String getName(){
        return this.name; 
    } 
    
    /**
     * @return The AdminUser's password
     */
 
    public String getPassword(){
        return this.password; 
    } 
 
    // Setters 
 
    /**
     * Sets the AdminUser's name
     * @param name The AdminUser's name
     */
 
    public void setName( String name ){
        this.name = name; 
    } 
 
    /**
     * Sets the AdminUser's password
     * @param password The AdminUser's password
     */
 
    public void setPassword( String password ){
        this.password = password; 
    } 
 
    // Database functionality 
 
    /**
     * Loads the AdminUser from the database as long as the name is set
     */
 
    public void retrieve(){
        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select admimname, adminpassword from adminsusers where admimname = '" + name + "'" ); 
        while( result.next() ){ 
            name = result.getString( "ADMINNAME" ); 
            password = result.getString( "ADMINPASSWORD" ); 
            } 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error retrieving from database" );
        } 
    } 
} 
