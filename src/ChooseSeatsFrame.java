
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/*
 * ChooseSeatsFrame.java
 * 
 * The frame for the user to interact with when choosing 
 * seats for a showing.
 * 
 * @author Niral Patel
 * @version v1.0
 * 
 */
public class ChooseSeatsFrame extends JFrame{
    //various variables and GUI components
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    JLabel title = new JLabel( "Please choose a seat" );
    static String seatChosen = "";
    static int bookingID = 0;
    static Random random = new Random();
    static int showID = 0;
    static int filmtime = 0;
    static int screenB = 0;
    static String filmdate = null;
    
    /*
     * ChooseSeatsFrame contructor
     * 
     * @param   filmname    The film name
     * @param   filmTime    The film time
     * @param   screen      The screen for the showing
     * @param   filmname    The film date
     * @param   filmname    The showing ID
     */
    ChooseSeatsFrame( String filmname, int filmTime, int screen, String filmDate, int showId ){
        //Setting up the GUI frame and the positioning of all the components
        super( "Book Seats" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        showID = showId;
        filmtime = filmTime;
        filmdate = filmDate;
        screenB = screen;
        
        int xPos = 10;
        int yPos = 100;
      
        //creating the array of seats
        JButton[] seats = new JButton[92];
        String[] seatNumber = new String[]{"A1","A2","A3","A4","A5","A6","A7","A8","A9","A10","A11","B1","B2","B3","B4","B5","B6","B7","B8","B9","B10","B11","C1","C2","C3","C4","C5","C6","C7","C8","C9","C10","C11","D1","D2","D3","D4","D5","D6","D7","D8","D9","D10","D11","E1","E2","E3","E4","E5","E6","E7","E8","E9","E10","E11","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","G1","G2","G3","G4","G5","G6","G7","G8","G9","G10","G11","H1","H2","H3","H4","H5","H6","H7","H8","H9","H10","H11","H12","H13","H14","H15"};
        
        LinkedList<String> bookedSeats = new LinkedList();
        
        try{
        bookedSeats = DBConnection.getLLQuery( "select seatid from seats where screenid = " + screen + " and filmdate = '" + filmdate + "' and filmtime = " + filmtime + "", DBConnection.connection );
        }catch(Exception e){
            JOptionPane.showMessageDialog( null, "Error retrieving booked seats" );
        }

        for( String seatNum: bookedSeats ){
            arrayIter:for( int i = 0; i < 92; i++){
                if (seatNum.equals(seatNumber[i])){
                    seatNumber[i] = "";
                    break arrayIter;
                }
            }
        }
        
        //for loop iterating over seats to creating the cinema layout
        //with clickable buttons
        for( int i  = 0; i < 92; i++ ){
            if( !"".equals(seatNumber[i]) ){
            seats[i] = new JButton( seatNumber[i] );
            seats[i].setBounds(xPos,yPos,58,58);
            bGPanel.add(seats[i]);
            seats[i].setActionCommand(seatNumber[i]);
            //adding listener to seat buttons, so when clicked will
            //book a ticket for that seat
            seats[i].addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    String seatNumber = e.getActionCommand();
                    int userOption = JOptionPane.showConfirmDialog(null, "You want to book seat " + seatNumber );
                    ChooseSeatsFrame.seatChosen =  seatNumber ;
                    if ( userOption == 0 ){
                        try{ 
                            DBConnection.addQuery( "insert into seats (screenid, seatid, filmdate, filmtime) values(" + screenB + ", '"+ seatNumber +"', '" + filmdate + "', " + filmtime + ")",DBConnection.connection );
                            bookingID  = random.nextInt(100000);
                            Booking booking = new Booking( bookingID, showID ,  seatNumber , Login.loggedInUserName );
                            booking.add();
                            JOptionPane.showMessageDialog( null, "Your card has been charged.\nYour ticket has been booked" );
                            dispose();
                        }catch( Exception exception ){
                            JOptionPane.showMessageDialog( null, "Error Booking Seat" );
                        }  
                    }
                }
                });
            //setting the layout of the seat buttons
            }else{
                seats[i] = new JButton( seatNumber[i] );
                seats[i].setBackground(Color.RED);
                seats[i].setBounds(xPos,yPos,58,58);
                bGPanel.add(seats[i]);
            }

            if( yPos < 520 ){
                xPos = xPos + 60;
                if( xPos == 190 || xPos == 610 ){
                    xPos = xPos + 120;
                }
                if( xPos >= 863 ){
                  xPos = 10;
                  yPos = yPos + 60;
                }
            }else{
                xPos = xPos + 60;         
            }
        }
        
        JButton available = new JButton();
        JLabel unavailable = new JLabel();        
        JLabel avail = new JLabel( "Available" );
        JLabel moveieScreen = new JLabel();
        JLabel unavail = new JLabel( "Unavailable" );
        back.setBounds(1050,20,100,20);
        
        //adding and positioning components
        bGPanel.add(back);
        bGPanel.add( title );
        bGPanel.add( moveieScreen );
        bGPanel.add( available );
        bGPanel.add( unavailable );
        bGPanel.add( avail );
        bGPanel.add( unavail );
        title.setBounds(400,10,200,20);
        moveieScreen.setBounds(300,40,320,20);
        available.setBounds(980,450,58,58);
        unavailable.setBounds(980,520,58,58);
        avail.setBounds(1050,470,200,20);
        unavail.setBounds(1050,540,200,20);
        unavailable.setBackground(Color.RED);
        unavailable.setOpaque(true);
        moveieScreen.setBackground(Color.BLACK);
        moveieScreen.setOpaque(true);
        
        //adding scrollbar to frame
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setVisible(true);
        setSize(1200,650);
        setLocationRelativeTo(null);
        
        clickBack();
    } 
    
    /*
     * clickBack Method
     * 
     * method to dispose the frame when back button is clicked
     */
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }  
}
