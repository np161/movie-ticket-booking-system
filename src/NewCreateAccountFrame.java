import javax.swing.*;
import java.awt.event.*;

/**
 * NewCreateAccountFrame Class
 * Frame for creating new account.
 * @Author: Niral Patel
 * @version 1.0
 */

 public class NewCreateAccountFrame extends JFrame {
     JButton cancel = new JButton( "Cancel" );
     JLabel welcome = new JLabel( "Please enter a username and a password" );
     JPanel panel = new JPanel();
     JButton blogin = new JButton( "Make My Account" );
     JTextField txuser = new JTextField(15);
     JPasswordField pass = new JPasswordField(15);
     JTextField userCardDetails = new JTextField(20);
     JLabel labelUser = new JLabel( "Username" );
     JLabel labelPassword = new JLabel( "Password" );
     JLabel labelUserCardDetails = new JLabel( "Credit-Card Number" );

     //Add content to panel
     NewCreateAccountFrame(){
         super("Create Account");
         setSize(450,250);
         setLocationRelativeTo(null);
         panel.setLayout (null);

         welcome.setBounds(110,5,300,60);
         txuser.setBounds(160,50,150,20);
         pass.setBounds(160,85,150,20);
         cancel.setBounds(325,150,100,20);
         userCardDetails.setBounds(160,120,150,20);
         blogin.setBounds(160,150,150,20);
         labelUser.setBounds(25,50,100,20);
         labelPassword.setBounds(25,85,100,20);
         labelUserCardDetails.setBounds(25,120,150,20);
         pass.setToolTipText("Minimum of 8 characters. Must contain a capital letter and a digit");
         
         panel.add( cancel );
         panel.add( welcome );
         panel.add( blogin );
         panel.add( txuser );
         panel.add( userCardDetails );
         panel.add( pass );
         panel.add( labelUser );
         panel.add( labelPassword );
         panel.add( labelUserCardDetails );

         getContentPane().add(panel);
         setVisible(true);
         actionLogin();
         clickCancel();
     }
     
     //Attempt to create account using input user & pass, checks if valid and informs user if error
     public void actionLogin(){
            
            blogin.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    boolean valid  = false;
                    String userName = txuser.getText();
                    if( userName.equals("") ){
                        JOptionPane.showMessageDialog( null, "Please enter a valid username" );
                    }else if( DBConnection.inDB(userName) == true ){
                        JOptionPane.showMessageDialog( null, "Username already taken" );
                    }else{
                        userName = userName.replace("\"", "");
                        String userPassword = pass.getText();
                        try{
                            Checks.userInputCheck(userPassword , ".*[A-Z].*[0-9].*|.*[0-9].*[A-Z].*");
                            valid = true;
                        }catch( Exception e ){
                            JOptionPane.showMessageDialog( null, "Please enter a valid password" );
                        }
                        if( valid == true ){
                        if( userPassword.equals("") || userPassword.length() < 8 || userPassword.length() > 32 ){
                            JOptionPane.showMessageDialog( null, "Please enter a valid password" );
                        }else{
                            userPassword = userPassword.replace("\"", "");
                            String cardNumber = userCardDetails.getText();
                            if( cardNumber.length() < 16 || cardNumber.length() > 16 ){
                                JOptionPane.showMessageDialog( null, "Please enter a valid card number" );
                            }else{
                                Long userCreditCardDetails = Long.parseLong( userCardDetails.getText() );
                                User member = new User( userName, userPassword, userCreditCardDetails );
                                member.addNewMember();
                                JOptionPane.showMessageDialog( null,"Your account has been created" );
                                dispose();
                            }
                        }
                        }
                    }
                }
            });
 }
     public void clickCancel(){
        cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
         });
     }
 }
