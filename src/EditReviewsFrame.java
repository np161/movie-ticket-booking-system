
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * EditReviewsFrame Class
 * Frame for administator to edit a filmscurrent reviews
 * @Author: Niral Patel
 * @version 1.0
 */
 
public class EditReviewsFrame extends JFrame{
    //Setup swing elements for frame
    ImageIcon image = new ImageIcon( "cancel.jpg" );
    ImageIcon image1 = new ImageIcon( "save changes.jpg" );
    JTextArea newsletter;
    JLabel title = new JLabel( "Please enter review here" );
    JButton save = new JButton( image1 );
    JButton cancel = new JButton( image );
    Review review;
     
    EditReviewsFrame( Review reviewObject ){
        super( "Edit Review" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        this.review = reviewObject;
        newsletter = new JTextArea( reviewObject.getReviewText() );
        
        //Set bounds for swing elements
        title.setBounds(100,10,300,20);
        save.setBounds(100,270,150,20);
        newsletter.setBounds(100,50,500,200);
        cancel.setBounds(500,270,100,20);
        
        bGPanel.add( cancel );
        bGPanel.add( title );
        bGPanel.add( newsletter );
        bGPanel.add( save );
        
        JScrollPane scroller = new JScrollPane(newsletter, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroller.setBounds(100,50,500,200);
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(750,400);
        setLocationRelativeTo(null);
        setVisible(true);     
        
        clickSave();
        clickCancel();
    }
    
    //Button to allow admin to save reviews edited contents, updating review object and saving into database
    public void clickSave(){
        save.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    String newText = newsletter.getText();
                    review.setReviewText( newText );
                    review.save();
                    JOptionPane.showMessageDialog( null, "The changes have been saved" );
                    dispose();
                }
        });
    }
    
    public void clickCancel(){
        cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
                });
        
    }
}
