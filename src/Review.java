import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Review class
 * Contains information relating to reviews
 * @version 1.0
 */
public class Review {
    private String filmName;
    private String reviewText;
    private String userCreator;
    
    /**
    * Default constructor
    */
    Review(){}
    
    /**
     * Constructor
     * @param filmName The name of the film associated with the review
     * @param reviewText The text for the Review
     * @param userCreator The username of the creator of the Review
     */
    Review( String filmName, String reviewText, String userCreator ){
        this.filmName = filmName;
        this.reviewText = reviewText;
        this.userCreator = userCreator;
    }
    
    // Getters for this class:
    
    /**
     * @return The name of the associated film
     */
    public String getFilmName(){
        return this.filmName;
    }
    
    /**
     * @return The text for the review
     */
    public String getReviewText(){
        return this.reviewText;
    }
    
    /**
     * @return The username of the creator of the Review
     */
    public String getUserCreator(){
        return this.userCreator;
    }
    
    // Setters for this class:
    
    /**
     * Sets the name of the associated film
     * @param filmName The name of the film
     */
    public void setFilmName( String filmName ){
        this.filmName = filmName;
    }
    
    /**
     * Sets the text for the Review
     * @param reviewText The text
     */
    public void setReviewText( String reviewText ){
        this.reviewText = reviewText;
    }
    
    /**
     * Sets the username of the Review creator
     * @param The username
     */
    public void setUserCreator( String userCreator ){
        this.userCreator = userCreator;
    }
    
    // Database functionality for this class:
    
    /**
     * Loads information from the database
     * @param filmname The film reviews to retrieve
     * @param username The username to retrieve reviews for
     */
    public void retrieve( String filmname, String username ){

        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select filmname, reviewtext, username from review where filmname = '" + filmname + "' and username = '" + username + "'" ); 

            while( result.next() ){ 
            filmName = result.getString( "FILMNAME" ); 
            reviewText = result.getString( "REVIEWTEXT" ); 
            userCreator = result.getString( "USERNAME" ); } 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error with Database" );
        } 

    } 

    /** 
     * Update Review to the database
     */
    public void save(){
        try{
            DBConnection.addQuery( "update review set reviewtext = '" + reviewText + "' where username = '" + userCreator + "' and filmname = '" + filmName + "'", DBConnection.connection );
        }catch( Exception e){
            JOptionPane.showMessageDialog( null, "Error with Database" );
        }  
    }
    
    /** 
     * Adds new Review to the database
     */
    public void add(){
        try{
            DBConnection.addQuery( "insert into review values('" + filmName + "','" + reviewText + "','" + userCreator + "')" , DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error with Database" );
        } 
    }
    
     /** 
     * Checks whether review has been written already
     * @return boolean result 
     */
    public boolean checkWritten(){
        Boolean result = false; 
        String film = "";
        String dbUser = "";
       
        try{
        Statement statement = DBConnection.connection.createStatement();
        ResultSet dbResult = statement.executeQuery( "select filmname, username from review where filmname = '" + filmName + "' and username = '" + userCreator + "'" );
 
        while( dbResult.next() ){
            film = dbResult.getString( "FILMNAME" );
            dbUser = dbResult.getString( "USERNAME" );
            }
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error checking Database" );
        } 
        if( filmName.equals( film ) && userCreator.equals( dbUser ) ){
            result = true;
        } 
        return result;
    }
}
