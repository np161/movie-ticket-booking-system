import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter; 
import javax.swing.JOptionPane;

/**
 * Ticket Class
 * Stores information relating to Tickets
 * @version 1.0
 */
public class Ticket {
    
    private int bookingID;
    private int screenID;
    private String seatID;
    private String filmName;
    private String username;
    private int filmTime;
    private String filmDate;
    
    /**
    * Default constructor
    */
    Ticket(){
    }
    
    /**
     * Constructor for ticket object
     * @param bookingID The ID number of the associated booking
     * @param screenID The ID number of the screen
     * @param seatID The ID number of the associated seat
     * @param filmName The name of the film
     * @param username The username of the Ticket owner
     * @param filmTime The time of the showing
     */
    Ticket( int bookingID, int screenID, String seatID, String filmName, String username, int filmTime, String filmDate ){
        this.bookingID = bookingID;
        this.screenID = screenID;
        this.seatID = seatID;
        this.filmName = filmName;
        this.username = username;
        this.filmTime = filmTime;
        this.filmDate = filmDate;
    }
    
    // Getters for this class:
    
    /**
     * @return The ID number of the associated Booking
     */
    public int getBookingID(){
        return this.bookingID;
    }
    
    /**
     * @return The screen ID number
     */
    public int getScreenID(){
        return this.screenID;
    }
    
    /**
     * @return The seat ID number
     */
    public String getSeatID(){
        return this.seatID;
    }
    
    /**
     * @return The name of the film
     */
    public String getFilmName(){
        return this.filmName;
    }
    
    /**
     * @return The username of the Ticket owner
     */
    public String getUsername(){
        return this.username;
    }
    
    /**
     * @return The time of the showing
     */
    public int getFilmTime(){
        return this.filmTime;
    }
    
     /**
     * @return The data of the showing
     */
    public String getFilmDate(){
        return this.filmDate;
    }
    
    // Setters for this class:
    
    /**
     * Sets the ID number of the associated booking
     * @param bookingID The associated booking ID
     */
    public void setBookingID( int bookingID ){
        this.bookingID = bookingID;
    }
    
    /**
     * sets the associated screen ID number
     * @param screenID The screen ID number
     */
    public void setScreenID( int screenID ){
        this.screenID = screenID;
    }
    
    /**
     * Sets the seat ID number
     * @param seatID The seat ID number
     */
    public void setSeatID( String seatID ){
        this.seatID = seatID;
    }
    
    /**
     * Sets the associated film name
     * @param filmName The associated film name
     */
    public void setFilmName( String filmName ){
        this.filmName = filmName;
    }
    
    /**
     * Sets the associated username
     * @param username The associated username
     */
    public void setUsername( String username ){
        this.username = username;
    }
    
    /**
     * Sets the time of the showing
     *@param filmTime The time of the showing
     */
    public void setFilmTime( int filmTime ){
        this.filmTime = filmTime;
    }
    
     /**
     * Sets the date of the showing
     *@param filmDate The date of the showing
     */
    public void setFilmDate( String filmDate ){
        this.filmDate = filmDate;
    }
    
    // Database functionality for this class:
    
    /**
     * Prints the ticket into a file
     */
    public void printTicket(){

       Ticket ticket = new Ticket( bookingID, screenID, seatID, filmName, username, filmTime, filmDate ); 
       File newFile = new File( "Ticket.txt" );
       newFile.delete();
       newFile = new File( "Ticket.txt" );

        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter( ( "Ticket.txt"), true));
            writer.write( "Film: " + filmName + "\n" ); 
            writer.write( "Date: " + filmDate + "\n" );
            writer.write( "Time: " + filmTime + "\n" );
            writer.write( "Screen ID: " + screenID + "\n" );
            writer.write( "Seat ID: " + seatID + "\n" ); 
            writer.write( "Name: " + username + "\n" ); 
            writer.write( "Booking ID: " + bookingID + "\n" ); 
            writer.close(); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Problem Printing" );
        } 
    } 

    /**
     * Loads information from database for a given Booking ID
     * @param bookingID The ID number of the associated booking
     * */
     
    public void retrieve( int bookingID ){

        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select bookingid, filmname, filmtime, screenid, seatid, username from bookings where bookingid = '" + bookingID + "'" ); 

        while( result.next() ){ 
            bookingID = result.getInt( "BOOKINGID" ); 
            screenID = result.getInt( "SCREENID" ); 
            seatID = result.getInt( "SEATID" ); 
            username = result.getString( "USERNAME" ); 
            filmName = result.getString( "FILMNAME" ); 
            filmTime = result.getInt( "FILMTIME" ); 
        } 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error retrieving ticket" );
        } 
    } 

    /**
     * Saves the Ticket to the database
     */
    public void save(){
        try{ 
        DBConnection.addQuery( "update bookings set bookingid = " + bookingID + ", screenid = " + screenID + ", seatid = " + seatID + ", username = '" + username + "', filmname = '" + filmName + "', filmtime = " + filmTime + " where bookingid = " + bookingID, DBConnection.connection); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Problem Saving" );
        } 
    }
}