import java.sql.*;
import java.util.LinkedList;
import javax.swing.JOptionPane;
 
/**
 * DBConnection.java
 * 
 * The object which controls and creates access to the database
 * 
 * @author Niral Patel
 * @version 1.0
 */
 
public class DBConnection{
     static Connection connection = null;

    /**
     * Connects to the database and gets the corresponding Connection object
     * @return The Connection
     * @throws Exception 
     */
    public static void makeConnection(){
        try{
            Class.forName( "oracle.jdbc.driver.OracleDriver" );
	}catch(ClassNotFoundException e) {
            JOptionPane.showMessageDialog( null, "Driver Error" );
            System.exit(0);
	}
        try{
            connection = DriverManager.getConnection( "jdbc:oracle:thin:@secam-oracle.ex.ac.uk:2424:CSPR","username","password" );
        } catch(SQLException e){
            JOptionPane.showMessageDialog( null, "Connection failed" );
        }
    }
 
    /**
     * Returns results of a given SQL query
     * @param query The SQL query to execute
     * @param resultConnection The Connection object
     * @return The result of the query in the form of a string
     * @throws Exception 
     */
    public static String getQuery( String query, Connection resultConnection ) throws Exception{
        Statement statement = resultConnection.createStatement(); 
        ResultSet result = statement.executeQuery( query );
 
        String dbResult = "";
 
        while( result.next()){
            dbResult = result.getString(1);
        }
            return dbResult;
        }
 
    /**
     * Returns results of a given SQL query in a String LinkedList format
     * @param query The SQL query to execute
     * @param resultConnection The Connection object
     * @return The results of the query in the form of a LinkedList of Strings
     * @throws Exception 
     */
    public static LinkedList<String> getLLQuery( String query, Connection resultConnection ) throws Exception{
        Statement statement = resultConnection.createStatement(); 
        ResultSet result = statement.executeQuery( query );
        
        LinkedList<String> dbResult = new LinkedList();

        while( result.next()){
            dbResult.add( result.getString(1) );
        }
        return dbResult;
    }
 
    /**
     * Returns results of a given SQL query in a Integer LinkedList format
     * @param query The SQL query to execute
     * @param resultConnection The Connection object
     * @return The results of the query in the form of a LinkedList of Integers
     * @throws Exception 
     */
    public static LinkedList<Integer> getLLIntQuery( String query, Connection resultConnection ) throws Exception{
        Statement statement = resultConnection.createStatement(); 
        ResultSet result = statement.executeQuery( query );
 
        LinkedList<Integer> dbResult = new LinkedList();

        while( result.next()){
            dbResult.add( result.getInt(1) );
        }

        return dbResult;
    }
 
    /**
     * Executes an SQL 'INSERT' query in the database
     * @param query The SQL query to execute
     * @param queryConnection The Connection object
     * @throws Exception 
     */
 
    public static void addQuery( String query, Connection queryConnection ) throws Exception {
        Statement statement = queryConnection.createStatement(); 
        ResultSet result = statement.executeQuery( query );
        }

    /**
     * Adds a Booking to the database
     * @param bookingID The booking ID
     * @param showID The show ID
     * @param seatNumber The seat number
     * @param userName The username
     */
 
    public static void addBooking( int bookingID, int showID, String seatNumber, String userName ){
        try{
            DBConnection.addQuery( "insert into bookings values(" + bookingID + "," + showID + "," + seatNumber + ",'" + userName + "')", DBConnection.connection );
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error with database" );
        }
    }
 
    /**
     * Checks if a username has already been used and stored in the database
     * for logging a user in
     * @param userName The username to check for
     * @param password The password to check against
     * @return Boolean value representing whether the user and the
     * corresponding password are in the database
     */
    public static boolean checkUserDetails( String userName, String password ){
        boolean inDB = false;
        String dbUserName = "";
        String dbUserPassword = "";
        try{
            Statement statement = DBConnection.connection.createStatement();
            ResultSet result = statement.executeQuery( "select username, userpassword from users where username = '" + userName + "'" );
 
            while( result.next() ){
                dbUserName = result.getString( "USERNAME" );
                dbUserPassword = result.getString( "USERPASSWORD" );
            }
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error with database" );
        }
    
        if( userName.equals( dbUserName ) && password.equals( dbUserPassword ) ){
            inDB = true;
        } 
        return inDB;
     } 
    
    /**
     * Checks if a username has already been used and stored in the database
     * for creating new accounts
     * @param userName The username to check for
     * 
     * @return Boolean value representing whether the user is in the database
     */
    public static boolean inDB( String userName ){
        boolean inDB = false;
        String dbUserName = "";
        try{
            Statement statement = DBConnection.connection.createStatement();
            ResultSet result = statement.executeQuery( "select username from users where username = '" + userName + "'" );
 
            while( result.next() ){
                dbUserName = result.getString( "USERNAME" );
                if( userName.equals( dbUserName ) ){
                    inDB = true;
                }
            }
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error with database" ); 
        }

        return inDB;
    } 
 
    /**
     * Checks if an Admin username has already been used and stored in the database
     * @param userName The Admin username to check for
     * @return Boolean value representing whether the username is in the database
     */
    public static boolean checkAdminUserDetails( String adminName, String password ){
        boolean inDB = false;
        String dbAdminName = "";
        String dbAdminPassword = "";

        try{
            Statement statement = DBConnection.connection.createStatement();
            ResultSet result = statement.executeQuery( "select adminname, adminpassword from adminuser where adminname = '" + adminName + "'" );

            while( result.next() ){
                dbAdminName = result.getString( "ADMINNAME" );
                dbAdminPassword = result.getString( "ADMINPASSWORD" );
            }
         }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error with database" ); 
         }

         if( adminName.equals( dbAdminName ) && password.equals( dbAdminPassword ) ){
            inDB = true;
         } 
         
         return inDB;
     }
}

 
