
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

/**
 * ViewReviewsFrame Class
 * Allows the user to view the reviews for a film
 * @author Niral Patel
 */
 
public class ViewReviewsFrame extends JFrame{
    // Set up Swing components
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    LinkedList<String> reviews = new LinkedList();
    LinkedList<String> users = new LinkedList();
    ArrayList<Review> reviewObjects = new ArrayList();
    int reviewYPos = 50;
    int userYPos = 100;
    
    /**
    * Constructor
    * @param filmname The name of the film
    */
    ViewReviewsFrame( String filmname ){
    
        // Initially set up new panel
        super( filmname + " Reviews" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        try{
            //Retrieve reviews from database 
            reviews = DBConnection.getLLQuery("select reviewtext from review where filmname ='" + filmname + "'", DBConnection.connection);
            users = DBConnection.getLLQuery("select username from review where filmname ='" + filmname + "'", DBConnection.connection);
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving reviews" );
        }

        for( String string: reviews ){
            //For each review string:
            int usersIndex = reviews.indexOf( string );
            
            Review reviewObject = new Review(); //Initiate holder object
            
            string = string.replace( "\"", "" );
            reviewObject.retrieve( filmname, users.get(usersIndex) );
            reviewObjects.add( reviewObject ); //Add review to ArrayList of reviews
            
            JLabel review = new JLabel( "<html><body style='width:500px'>" + string );
            JLabel user = new JLabel( "Review by " + users.get(usersIndex)  ); //State who wrote review
            review.setBackground(Color.white);
            review.setOpaque( true );
            review.setBounds(20,reviewYPos,500,50);
            user.setBounds(220,userYPos,300,20);
            user.setHorizontalAlignment(SwingConstants.RIGHT);
            
            bGPanel.add( review ); //Add review content to panel
            bGPanel.add( user );   //Add review author to panel
            
            reviewYPos += 75; //Increment position for next review
            userYPos += 75; //Increment position for next review author
        }
        
        back.setBounds(420,10,100,20);
        bGPanel.add(back);
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(600,400);
        setLocationRelativeTo(null);
        setVisible(true);
        
        clickBack();
    } 
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }
}