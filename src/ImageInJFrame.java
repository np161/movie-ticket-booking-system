
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JScrollPane;

/**
 * ImageInJFrame Class
 * Home/Main frame for users once program executed, allows them to view available films and login.
 * @Author: Niral Patel
 * @version 1.0
 */

public class ImageInJFrame extends JFrame {
  //Setup swing elements for this frame
  ImageIcon image = new ImageIcon( "member login.jpg" );
  ImageIcon image1 = new ImageIcon( "exit.jpg" );
  ImageIcon image2 = new ImageIcon( "administrator login.jpg" );
  static LinkedList<String> filmImagesList = new LinkedList();
  static LinkedList<String> filmnameList = new LinkedList();
  static ArrayList<JButton> buttons = new ArrayList() ;
  static ArrayList<String> filmHyperlinks = new ArrayList();
  static ArrayList<Film> films = new ArrayList();
  JButton blogin = new JButton( image );
  JButton exit = new JButton( image1 );
  JButton bAdminLogin = new JButton( image2 );
  static Image backGround;
  BrowserButton panel;
  static ImageInJFrame mainFrame = new ImageInJFrame();
  
    //Main method to run once program is executed, loads swing frame for user to browse films
    public static void main( String args[]){
      try{
          DBConnection.makeConnection();
          filmImagesList = DBConnection.getLLQuery( "select filmposter from film order by filmname", DBConnection.connection );
          filmnameList = DBConnection.getLLQuery( "select filmname from film order by filmname", DBConnection.connection );
      }catch(Exception e){
          System.exit(0);
      }

      backGround = ReadImage.getImage("http://wallpapers.wallbase.cc/rozne/wallpaper-930107.png");
      
      mainFrame.start();
    }

      public void start(){
          Browser bGPanel = new Browser( backGround );
          add( bGPanel );

          int xPos = 50;
          int yPos = 75;

          //Adds film posters to the frame
          for( String link : filmImagesList ){
            Image filmImage = ReadImage.getImage( link );
            Image newFilmImage = filmImage.getScaledInstance(250, 400, java.awt.Image.SCALE_SMOOTH);
            panel = new BrowserButton( newFilmImage );
            buttons.add( panel );
            filmHyperlinks.add(link);
            panel.setBounds(xPos, yPos, 250, 400);
            bGPanel.add(panel);
            xPos = xPos + 300;
            if ( xPos >= 1800 ){
                xPos = 50;
                yPos = yPos + 450;
            }
          }
          
          for( String name : filmnameList ){
              Film film = new Film();
              film.retrieve( name );
              films.add( film );
          }
          
            for(int i=0; i< buttons.size(); i++){
            buttons.get(i).setActionCommand( filmnameList.get(i) );
            buttons.get(i).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String filmname = e.getActionCommand();
                for( Film film : films ){
                    if ( film.getFilmName().equals( filmname ) ){
                        FilmDetailsFrame filmInfo = new FilmDetailsFrame( film.getFilmName(), film.getPoster() , film.getSynopsis() );
                    }
                }
            }
            });
        }
          
        exit.setBounds(1700,10,100,20);
        blogin.setBounds(1500,10,150,20);
        bAdminLogin.setBounds(50,10,175,20);
        bGPanel.add(blogin);
        bGPanel.add(bAdminLogin);
        bGPanel.add( exit );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0,0,screenSize.width, screenSize.height);
        setVisible(true);

        setDefaultCloseOperation(EXIT_ON_CLOSE);
        clickLogin();
        clickAdminLogin();
        clickExit();
    }

    //Opens login frame for user to login, also allows to create account. Closes once user is logged in
    public void clickLogin(){
            blogin.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){ 
                    LoginFrame lFrame = new LoginFrame();
                    if( Login.loggedIn == true ){
                        lFrame.revalidate();
                        lFrame.repaint();
                        dispose();
                    }
                }
                });
    }

     //Opens login frame for admin to login. Closes once admin is logged in
    public void clickAdminLogin(){
            bAdminLogin.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    AdminLoginFrame lFrame = new AdminLoginFrame();
                    if( Login.loggedIn == true ){
                        dispose();
                    }                  
                } 
                });
        }
    
    public void clickExit(){
        exit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    System.exit(0);
                }
        });
    }
    }
  