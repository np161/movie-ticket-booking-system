import java.sql.ResultSet; 
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 * Film class
 * Contains information relating to films
 * @Author: Niral Patel
 * @version 1.0
 */
public class Film {
    private String filmName = "";
    private String synopsis = "";
    private String poster = "";
    
    /**
    * Default constructor
    */
    Film(){}
    
    /**
     *Constructor
     * @param filmName The name of the Film
     * @param synopsis The synopsis of the Film
     * @param poster The Film poster
     */
    Film( String filmName, String synopsis, String poster ){
        this.filmName = filmName;
        this.synopsis = synopsis;
        this.poster = poster;
    }
    
    // Getters for this class:
    
    /**
     * @return The Film name
     */
    public String getFilmName(){
        return this.filmName;
    }
    
    /**
     * @return The Film synopsis
     */
    public String getSynopsis(){
        return this.synopsis;
    }
    
    /**
     * @return The Film poster
     */
    public String getPoster(){
        return this.poster;
    }
    
    // Setters for this class:
    
    /**
     * Sets the Film name
     * @param filmName The name of the Film
     */
    public void setFilmName( String filmName ){
        this.filmName = filmName;
    }
    
    /**
     * Sets the Film synopsis
     * @param synopsis The Film synopsis
     */
    public void setSynopsis( String synopsis ){
        this.synopsis = synopsis;
    }
    
    /**
     * Sets the Film poster
     * @param poster The poster for the Film
     */
    public void setPoster( String poster ){
        this.poster = poster;
    }
    
    // Database functionality for this class:
    
    /**
     * Loads the information from the database as long as the Film name is set
     * @param filmQuery The film title of the film to retrieve
     */
    public void retrieve( String filmQuery ){
        try{ 
            Statement statement = DBConnection.connection.createStatement();
            ResultSet result = statement.executeQuery( "select filmname, filmsynopsis, filmposter from film where filmname = '" + filmQuery + "'" );

            while( result.next() ){
                filmName = result.getString( "filmname" );
                synopsis = result.getString( "filmsynopsis" );
                poster = result.getString( "filmposter" );
            }
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving film" );
        } 
    } 

    /**
     * Saves the Film object to the database
     */
    public void save(){
        try{ 
            DBConnection.addQuery( "update film set filmsynopsis = '" + synopsis + "' where filmname = '" + filmName + "' and filmposter = '" + poster + "'", DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error saving" );
        } 
    } 
}