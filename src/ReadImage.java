import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
 
public class ReadImage 
{	

    /**
     * Method for retrieving image from URL
     * @param URLLink The url to retrieve
      *@return image The image data retrieved
    */
    public static Image getImage( String URLLink ){
        
    	Image image = null;
        try {
            URL url = new URL( URLLink );
            image = ImageIO.read(url);
        } catch (IOException e) {
        	e.printStackTrace();
        }
        return image;
    }
}
