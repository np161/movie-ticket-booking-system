
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

/**
 * EditFilmReviewsFrame Class
 * Frame for administator to edit filmreviews
 * 
 * @Author: Niral Patel
 * @version 1.0
 */

public class EditFilmReviewsFrame extends JFrame{
    //Setup swing elements for frame
    LinkedList<String> reviews = new LinkedList();
    LinkedList<String> users = new LinkedList();
    ArrayList<JButton> buttons = new ArrayList();
    ArrayList<Review> reviewObjects = new ArrayList();
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    int reviewYPos = 50;
    int userYPos = 125;
    
    EditFilmReviewsFrame( String filmID ){
        super( "Choose Review" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        try{
            reviews = DBConnection.getLLQuery("select reviewtext from review where filmname ='" + filmID + "'", DBConnection.connection);
            users = DBConnection.getLLQuery("select username from review where filmname ='" + filmID + "'", DBConnection.connection);
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving reviews" );
        }
            
        for(String string: reviews){

            int usersIndex = reviews.indexOf(string);
            
            Review reviewObject = new Review();
            
            string = string.replace("\"", "");
            reviewObject.retrieve( filmID, users.get(usersIndex) );
            reviewObjects.add( reviewObject );
            
            
            JButton review = new JButton( string );
            buttons.add( review );
            JLabel user = new JLabel( "Review by " + users.get(usersIndex)  );

            review.setBounds(50,reviewYPos,500,75);
            user.setBounds(250,userYPos,300,20);
            user.setHorizontalAlignment(SwingConstants.RIGHT);
            
            
            bGPanel.add( review );
            bGPanel.add( user );  
            
            reviewYPos += 95; //Increment review contents position in frame
            userYPos += 95; //Increment review author position in frame
        }

        for(int i=0; i< buttons.size(); i++){
            buttons.get(i).setActionCommand( "" + i  );
            buttons.get(i).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String index = e.getActionCommand();
                int newIndex = Integer.parseInt( index );
                EditReviewsFrame frame = new EditReviewsFrame( reviewObjects.get(newIndex) );
                dispose();
                }
            });
        }
        
        //Set bounds for swing elements
        back.setBounds(450,10,100,20);
        label.setBounds(120,10,300,20);
        
        JLabel label = new JLabel( "Click a review to edit it" );
        bGPanel.add(label);
        bGPanel.add(back);
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(625,400);
        setLocationRelativeTo(null);
        setVisible(true);
        clickBack();
    }
    
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }
}
