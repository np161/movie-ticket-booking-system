import java.sql.ResultSet; 
import java.sql.Statement;
import java.util.LinkedList;
import javax.swing.JOptionPane;
 
/**
 * Booking class contains information relating to bookings
 *
 * @Author: Niral Patel
 * @version 1.0
 */
 
public class Booking {
    private int bookingID; 
    private int showID; 
    private String seatID; 
    private String username; 
 
    Booking(){} 
 
    /**
     *Constructor
     * @param bookingID The ID number for this booking
     * @param showID The ID number of the corresponding show
     * @param seatID The ID number of the seat booked
     * @param username The username associated with the booking
     */
 
    Booking( int bookingID, int showID, String seatID, String username ){
        this.bookingID = bookingID; 
        this.showID = showID; 
        this.seatID = seatID; 
        this.username = username; 
    } 

    // Getters 
 
    /**
     *@return The booking ID for this Booking
     */
 
    public int getBookingID(){
        return this.bookingID; 
    } 
 
    /**
     * @return The show ID for this Booking
     */ 
 
    public int getShowID(){
        return this.showID; 
    } 
 
    /**
     * @return The seat ID for this Booking
     */ 
 
    public String getSeatID(){
        return this.seatID; 
    } 
 
    /**
     * @return The username for this Booking
     */
 
    public String getUsername(){
        return this.username; 
    } 
 
    // Setters 
 
    /**
     * Sets the ID number for this Booking
     * @param bookingID The ID number
     */
 
    public void setBookingID( int bookingID ){
        this.bookingID = bookingID; 
    } 
 
    /**
     * Sets the ID number of the show corresponding to this Booking
     * @param showID The show ID number
     */
 
    public void setShowID( int showID ){
        this.showID = showID; 
    } 
 
    /**
     * Sets the ID number of the seat for this Booking
     * @param seatID The ID number of the seat
     */
 
    public void setSeatID( String seatID ){
        this.seatID = seatID; 
    } 
 
    /**
     * Sets the username of the user who owns this Booking
     * @param username The username of the Booking owner
     */
 
    public void setUsername( String username ){
        this.username = username; 
    } 
 
    // Database functionality 
 
    /**
     * Loads the information from the database as long as the Booking ID is set
     * 
     * @param booknID   id search number
     */ 
    public void retrieve( int booknID ){
        try{ 
            Statement statement = DBConnection.connection.createStatement();
            ResultSet result = statement.executeQuery( "select bookingid, showid, seatid, username from bookings where bookingid = " + booknID ); 
            while( result.next() ){ 
                bookingID = result.getInt( "BOOKINGID" ); 
                showID = result.getInt( "SHOWID" ); 
                seatID = result.getString( "SEATID" ); 
                username = result.getString( "USERNAME" ); 
            }
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error retrieving from database" );
        } 
    }
    
    /*
     *Method to return a list of all bookingIDs related to a user
     *@param userName   the database search string
     *@return LinkedList<Integer> userBookingIDs
     */
      public static LinkedList<Integer> retrieveIDs( String userName ){
      LinkedList<Integer> userBookingIDs = new LinkedList();
 
      try{
          userBookingIDs = DBConnection.getLLIntQuery( "select bookingid from bookings where username = '" + userName + "'", DBConnection.connection );
      }catch ( Exception e ){
          JOptionPane.showMessageDialog( null, "Error in database" );
      }
      return userBookingIDs;
      }
      
    /*
     *Method to return a list of all bookingIDs related to a user
     *@param userName  the database search string
     *@return LinkedList<Integer> userBookingIDs
     */
      public static LinkedList<Integer> retrieveShowIDs( String userName ){
      LinkedList<Integer> userShowIDs = new LinkedList();
 
      try{
        userShowIDs = DBConnection.getLLIntQuery( "select showid from bookings where username = '" + userName + "'", DBConnection.connection );
      }catch ( Exception e ){
          JOptionPane.showMessageDialog( null, "Error in database" );
      }
      return userShowIDs;
      }

    /**
     * save method
     * 
     * updates an already saved booking in the database
     */
    public void save(){
        try{
            DBConnection.addQuery( "update bookings set bookingid = " + bookingID + ", showid = " + showID + ", seatid = '" + seatID + "', username = '" + username + "' where bookingid = " + bookingID , DBConnection.connection ); 
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error in database" );
        } 
    } 

    /**
     * add method
     * 
     * adds a new booking to the database
     */
    public void add(){
        try{
            DBConnection.addQuery( "insert into bookings values (" + bookingID + ", " + showID + ", '" + seatID + "', '" + username + "')" , DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error in database" );
        } 
    } 
}








 
