
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 * EditMemberFrame Class
 * Frame for administator to edit users details
 * @Author: Niral Patel
 * @version 1.0
 */

public class EditMemberFrame extends JFrame{
    //Setup swing elements for frame
    JLabel memberName = new JLabel( "Member Name" );
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    JTextField userName = new JTextField();
    JPasswordField password = new JPasswordField();
    JLabel labelPassword = new JLabel( "Password" );
    JButton bOkay = new JButton( "Ok" );
    String testName;
    
    
    EditMemberFrame(){
        super( "Choose Member" );
        Image bGround = ImageInJFrame.backGround;
        Browser bGPanel = new Browser( bGround );
        add(bGPanel);

        //Set bounds for swing elements
        back.setBounds(350,20,100,20);
        userName.setBounds(200,75,150,20);
        password.setBounds(200,110,150,20);
        memberName.setBounds(75,75,100,20);
        labelPassword.setBounds(75,110,100,20);
        bOkay.setBounds(225,150,50,20);
        
        bGPanel.add( back );
        bGPanel.add( password );
        bGPanel.add( labelPassword );
        bGPanel.add( memberName );
        bGPanel.add( userName );
        bGPanel.add( bOkay );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(500,300);
        setLocationRelativeTo(null);
        setVisible(true);
        clickOK();
        clickBack();
    }
    
    //Updates new detailsfor selected user via input in textboxes
    public void clickOK(){
        bOkay.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    testName = userName.getText();
                    String searchName = testName.replace("\"", "");
                    String dbUserName = "";
                    if( testName.equals("") ){
                        JOptionPane.showMessageDialog( null, "Please enter a name" );
                    }else if( DBConnection.checkUserDetails( testName, password.getText() ) == true ){
                        EditMemberDetailsFrame details = new EditMemberDetailsFrame( testName, password.getText() );
                        dispose();
                    }else if( DBConnection.checkUserDetails( testName, password.getText() ) == false ){
                        JOptionPane.showMessageDialog( null, "Incorrect username or password" );
                    }
                }
                });
    }
    
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }  
}
