
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JButton;
import javax.swing.JPanel;

/*
 * Browser.java
 * 
 * A class containing the Browser and BrowserButton
 * classes for adding images to GUI panels and buttons
 * 
 * @author: Niral Patel
 * @version: v1.0
 * 
 */

/*
 * Browser class
 * 
 * A [@link JPanel] which takes an image as and 
 * adds it to the panel
 */
class Browser extends JPanel{
      private Image img;
      /*
       * Browser construtor
       * @param img     the image to be added to the panel
       */
      Browser(Image img){
        this.img = img;
        Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);
        setSize(size);
        setLayout(null);
      }
 
      public void paintComponent(Graphics g){
        g.drawImage(img, 0, 0, null);
      }
    }

/*
 * BrowserButton class
 * 
 * A [@link JButton] which takes an image as and 
 * adds it to the button
 */
class BrowserButton extends JButton{
      private Image img;
      /*
       * BrowserButton construtor
       * @param img     the image to be added to the button
       */
      BrowserButton(Image img){
        this.img = img;
        setLayout(null);
      }
 
      public void paintComponent(Graphics g){
        g.drawImage(img, 0, 0, null);
      }
 
    }
    
