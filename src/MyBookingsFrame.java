
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * MyBookingsFrame Class
 * Frame for users viewing this current bookings.
 * @Author: Niral Patel
 * @version 1.0
 */
 
public class MyBookingsFrame extends JFrame{
    LinkedList<Integer> bookingIDs = new LinkedList();
    LinkedList<Integer> showIDs = new LinkedList();
    LinkedList<Booking> bookings = new LinkedList();
    LinkedList<Showing> showings = new LinkedList();
    LinkedList<JButton> buttons = new LinkedList();
    JButton tempButton = new JButton();
    ImageIcon image = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image );
    JLabel click = new JLabel( "Click a booking to print the ticket" );
    int index;
    int indexButton;
    
    MyBookingsFrame( String username ){
        super( "My Bookings" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        int xPos = 10;
        int yPos = 75;
        
        try{
            bookingIDs = Booking.retrieveIDs(username); //Retrieve all bookings for current user
            showIDs = Booking.retrieveShowIDs(username); //Retrieve all showings booked for current user
        
            for( int id : bookingIDs ){
                Booking booking = new Booking();
                booking.retrieve(id);
                bookings.add( booking );
            }
            for( int id : showIDs ){
                Showing showing = new Showing();
                showing.retrieveShowings( id );
                showings.add( showing );
            }
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving showings" );
        }
        
        for( int i = 0; i < showings.size(); i++ ){
            //For each showing booked, display to user:
            
            JButton bookingDetails = new JButton( "<html><p>Date: " + showings.get(i).getFilmDate() + " <b>Time: " + showings.get(i).getFilmTime() + " <b>Film: " + showings.get(i).getFilmName() + " <b>Screen: " + showings.get(i).getScreen() + " <b>Seat: " + bookings.get(i).getSeatID() +  "</p></html>"   );
            buttons.add( bookingDetails );
            buttons.get(i).setActionCommand( ""+i );
            buttons.get(i).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int bookingsIndex = Integer.parseInt( e.getActionCommand() );
                //indexButton = buttons.indexOf( buttons.get(index) );
                int userOption = JOptionPane.showConfirmDialog(null, "Print Ticket?" );
                    if ( userOption == 0 ){
                        Ticket ticket = new Ticket( bookings.get(bookingsIndex).getBookingID(), showings.get(bookingsIndex).getScreen(), bookings.get(bookingsIndex).getSeatID(), showings.get(bookingsIndex).getFilmName(), Login.loggedInUserName, showings.get(bookingsIndex).getFilmTime(), showings.get(bookingsIndex).getFilmDate() );
                        ticket.printTicket();
                        JOptionPane.showMessageDialog(null, "The ticket has been printed" );
                        dispose();
                    }
            }
            });
            bookingDetails.setBounds(xPos, yPos, 150, 100);
            bGPanel.add( bookingDetails );
            xPos = xPos + 170;
            if( xPos > 670 ){
                xPos = 10;
                yPos = yPos + 110;
            }
        }
 
        back.setBounds(570,20,100,20);
        click.setBounds(250,20,300,20);
        bGPanel.add( back );
        bGPanel.add( click );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setBounds(0,0,720,400);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        add(pane);
        
        setVisible(true);
        setSize(720,400);
        setLocationRelativeTo(null);
        
        clickBack();
    }
    
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }
}
