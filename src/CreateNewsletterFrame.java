
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * CreateNewsLetterFrame class
 * 
 * GUI frame for the admin to use to create and publish
 * a new newsletter
 * 
 * @author Niral Patel
 * @version v1.0
 * 
 */
public class CreateNewsletterFrame extends JFrame{
    //various variable and image files
    ImageIcon image = new ImageIcon( "cancel.jpg" );
    ImageIcon image1 = new ImageIcon( "create and publish.jpg" );
    ImageIcon image2 = new ImageIcon( "create newsletter.jpg" );
    ImageIcon image3 = new ImageIcon( "edit current newsletter.jpg" );
    JButton cancel = new JButton( image );
    JButton createNew = new JButton( image1 );
    JTextArea newsletter = new JTextArea( "Enter newsetter text here" );
    JButton create = new JButton( image2 );
    JButton edit = new JButton( image3 );
    
    /*
     * CreateNewsletterFrame constrcstor
     * 
     * creates a CreateNewsLetterFrame for the admin to use
     */
    CreateNewsletterFrame(){
        //adding and positioning the components
        super( "Create Newsletter" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        newsletter.setBounds(50,50,650,300);
        createNew.setBounds(50,370,200,20);
        create.setBounds(50,20,150,20);
        edit.setBounds(500,20,200,20);
        cancel.setBounds(600,370,100,20);
        
        bGPanel.add( cancel );
        bGPanel.add(create);
        bGPanel.add(edit);
        bGPanel.add( createNew );
        
        JScrollPane sp = new JScrollPane( newsletter );
        sp.setBounds(50,50,650,300);
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(sp);
        add(pane);
    
        setSize(800,450);
        setLocationRelativeTo(null);
        setVisible(true);
        
        //button methods which are called when buttons
        //are clicked
        clickCreate();
        clickEdit();
        clickCancel();
        clickPublish();
    }
    
    /*
     * clickCreate method
     * 
     * creates a CreateNewsLetterFrame when create is clicked
     */
    public void clickCreate(){
        create.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    CreateNewsletterFrame frame = new CreateNewsletterFrame();
                    dispose();
                }
        });
    }
    
    /*
     * clickEdit method
     * 
     * creates a EditNewsletterFrame when edit is clicked
     */
    public void clickEdit(){
        edit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    EditNewsletterFrame frame = new EditNewsletterFrame();
                    dispose();
                }
        });        
    }
    
    /*
     * clickCancel method
     * 
     * disposes the frame when cancel is clicked
     */
    public void clickCancel(){
        cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
                });
        
    }
    
    /*
     * clickPublish method
     * 
     * adds the new newsletter to the database for 
     * display
     */
    public void clickPublish(){
        createNew.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    String oldNews = "";
                    try{
                        oldNews = DBConnection.getQuery( "select newstext from newsletter", DBConnection.connection );
                    }catch( Exception e ){
                        JOptionPane.showMessageDialog( null, "Error with database" );
                    }
                    Newsletter news = new Newsletter();
                    news.setText( newsletter.getText() );
                    try{
                        news.save(); 
                        JOptionPane.showMessageDialog( null, "The changes have been saved" );
                        dispose();
                    }catch(Exception e){
                        JOptionPane.showMessageDialog( null, "Error saving" );
                    } 
                }
        });
    }     
}
