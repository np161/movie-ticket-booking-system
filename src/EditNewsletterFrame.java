
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * EditReviewsFrame Class
 * Frame for administator to edit the current newsletter
 * @Author: Niral Patel
 * @version 1.0
 */
public class EditNewsletterFrame extends JFrame{
    //Setup swing elements for frame
    ImageIcon image = new ImageIcon( "cancel.jpg" );
    ImageIcon image1 = new ImageIcon( "save changes.jpg" );
    ImageIcon image2 = new ImageIcon( "create newsletter.jpg" );
    ImageIcon image3 = new ImageIcon( "edit current newsletter.jpg" );
    JTextArea newsletter;
    JButton cancel = new JButton( image );
    JButton save = new JButton( image1 );
    JButton create = new JButton( image2 );
    JButton edit = new JButton( image3 );
    Newsletter news = new Newsletter();
    
    
    EditNewsletterFrame(){
        super("Edit Newsletter");
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        news.retrieve();
        newsletter = new JTextArea( news.getText() );
        
        //Set bounds for swing elements
        newsletter.setBounds(50,50,650,300);
        save.setBounds(50,370,150,20);
        create.setBounds(50,20,150,20);
        edit.setBounds(500,20,200,20);
        cancel.setBounds(600,370,100,20);
        newsletter.setLineWrap(true);
        
        bGPanel.add( cancel );
        bGPanel.add( create );
        bGPanel.add( edit );
        bGPanel.add( save );
        
        JScrollPane sp = new JScrollPane( newsletter );
        sp.setBounds(50,50,650,300);
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(sp);
        add(pane);
        
        setSize(800,450);
        setLocationRelativeTo(null);
        setVisible(true);
        clickCreate();
        clickEdit();
        clickSave();
        clickCancel();
    }
    
    //Creates new newsletter frame for admin
    public void clickCreate(){
        create.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    CreateNewsletterFrame frame = new CreateNewsletterFrame();
                    dispose();
                }
        });
    }
    
    //Edits new newsletter frame for admin
    public void clickEdit(){
        edit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    EditNewsletterFrame frame = new EditNewsletterFrame();
                    dispose();
                }
        });
                
    }
    
    public void clickSave(){
        save.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    String newText = newsletter.getText();
                    String oldNews = news.getText();
                    news.setText( newText );
                    try{
                        news.save();
                        JOptionPane.showMessageDialog( null, "The changes have been saved" );
                        dispose();
                    }catch( Exception e ){
                    JOptionPane.showMessageDialog( null, "Error saving in database" );
                    } 
                }
        });
    }
    
    public void clickCancel(){
        cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
                });
        
    }
    
}
