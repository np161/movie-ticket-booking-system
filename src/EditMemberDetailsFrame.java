
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 * EditMemberDetailsFrame Class
 * Frame for administator to edit users details
 * @Author: Niral Patel
 * @version 1.0
 */

public class EditMemberDetailsFrame extends JFrame{
    //Setup swing elements for frame
    ImageIcon image = new ImageIcon( "update.jpg" );
    ImageIcon image1 = new ImageIcon( "cancel.jpg" );
    JLabel title = new JLabel( "Please enter any new details" );
    JLabel memberName = new JLabel( "New Username" );
    JLabel memberPassword = new JLabel( "New Password" );
    JLabel memberCardDetails = new JLabel( "New Card Number" );
    JLabel minimum = new JLabel( "(between 8 and 32 chracters. Must contain a capital letter and a digit)" );
    JTextField tmemberName;
    JPasswordField tmemberPassword;
    JTextField tmemberCardDetails;
    JButton bUpdate = new JButton( image );
    JButton bCancel = new JButton( image1 );
    User user = new User();
    String newUsername;
    String password;
    Long cardDetails;
    String oldUsername;
    
    EditMemberDetailsFrame( String username, String password ){
        super( "New Details" );
        Image bGround = ImageInJFrame.backGround;
        Browser bGPanel = new Browser( bGround );
        add( bGPanel );
        
        oldUsername = username;
        user.retrieve( username );
        
        tmemberName = new JTextField( username );
        tmemberPassword = new JPasswordField( password );
        tmemberCardDetails = new JTextField( "" + user.getCardDetails() );
           
`       //Set bounds for swing elements           
        memberName.setBounds(70,80,150,20);
        memberPassword.setBounds(70,110,150,20);
        memberCardDetails.setBounds(70,140,150,20);
        tmemberName.setBounds(200,80,150,20);
        tmemberPassword.setBounds(200,110,150,20);
        minimum.setBounds(370,110,400,20);
        tmemberCardDetails.setBounds(200,140,175,20);
        bUpdate.setBounds(100,200,100,20);
        bCancel.setBounds(300,200,100,20);
        title.setBounds(200,20,300,20);
        
        bGPanel.add(title);
        bGPanel.add( minimum );
        bGPanel.add( tmemberName );
        bGPanel.add( tmemberPassword );
        bGPanel.add( tmemberCardDetails );
        bGPanel.add( memberName );
        bGPanel.add( memberPassword );
        bGPanel.add( memberCardDetails );
        bGPanel.add( bUpdate );
        bGPanel.add( bCancel );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(850,300);
        setLocationRelativeTo(null);
        setVisible(true);
        clickUpdate();
        clickCancel();
    }
    
    //Validates input from admin for new user details 
    public void clickUpdate(){
        bUpdate.addActionListener( new ActionListener(){
                boolean valid = false;
                public void actionPerformed( ActionEvent ae ){
                    boolean valid  = false;
                    String userName = tmemberName.getText();
                    if( userName.equals("") ){
                        JOptionPane.showMessageDialog( null, "Please enter a valid username" );
                    }else if( DBConnection.inDB(userName) == true ){
                        JOptionPane.showMessageDialog( null, "Username already taken" );
                    }else{
                        newUsername = userName.replace("\"", "");
                        String userPassword = tmemberPassword.getText();
                        try{
                            Checks.userInputCheck(userPassword , ".*[A-Z].*[0-9].*|.*[0-9].*[A-Z].*");
                            valid = true;
                        }catch( Exception e ){
                            JOptionPane.showMessageDialog( null, "Please enter a valid password" );
                        }
                        if( valid == true ){
                            if( userPassword.equals("") || userPassword.length() < 8 || userPassword.length() > 32 ){
                                JOptionPane.showMessageDialog( null, "Please enter a valid password" );
                            }else{
                                password = userPassword.replace("\"", "");
                                String cardNumber = tmemberCardDetails.getText();
                                if( cardNumber.length() < 16 || cardNumber.length() > 16 ){
                                    JOptionPane.showMessageDialog( null, "Please enter a valid card number" );
                                }else{
                                    cardDetails = Long.parseLong( tmemberCardDetails.getText() );  
                                    user.setUsername( newUsername );
                                    user.setPassword( password );
                                    user.setCardDetails( cardDetails ); 
                                    user.save( oldUsername );
                                    JOptionPane.showMessageDialog( null, "The details have been updated" );
                                    dispose();
                                }
                            }
                        }
                    }
                }
                });
    }  
    
    public void clickCancel(){
         bCancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
         });
     }
}
