
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * FilmDetailsFrame Class
 * Frame for users to view a films details,showings and synopsis.
 * @Author: Niral Patel
 * @version 1.0
 */

public class FilmDetailsFrame extends JFrame{
    //Setup swing elements for frame
    ImageIcon image = new ImageIcon( "view review.jpg" );
    ImageIcon image1 = new ImageIcon( "write review.jpg" );
    JLabel details;
    Browser poster;
    LinkedList<String> timeList = new LinkedList();
    ArrayList<JButton> bookTicket = new ArrayList();
    ArrayList<Showing> showings = new ArrayList();
    BrowserButton panel;
    int xPos = 10;
    int yPos = 250;
    String filmImage;
    JButton vreviews = new JButton( image );
    JButton wreviews = new JButton( image1 );
    ImageIcon image2 = new ImageIcon( "back.jpg" );
    JButton back = new JButton( image2 );
    JButton timeButton;
    String filmName;

    FilmDetailsFrame( String filmname, String hyperlink, String filmDetails ){
        super( filmname + " Details" );
        Image bGround = ImageInJFrame.backGround;
        Browser bGPanel = new Browser( bGround );
        add(bGPanel);
        
        
        this.filmName = filmname;
        this.filmImage = hyperlink;
        
        Image filmImage = ReadImage.getImage( hyperlink );
        Image newFilmImage = filmImage.getScaledInstance(130, 200, java.awt.Image.SCALE_SMOOTH);
        
        poster = new Browser( newFilmImage );
        details = new JLabel( "<html><body style='width:400px'>" + filmDetails );
        
        try{ 
        Statement statement = DBConnection.connection.createStatement(); 
        ResultSet result = statement.executeQuery( "select showid, filmname, filmtime, screenid, filmdate from showing where filmname = '" + filmname + "' order by filmname" ); 

        while( result.next() ){ 
             Showing specificShow = new Showing( result.getInt("SHOWID"), result.getString("FILMNAME"), result.getInt("FILMTIME"), result.getInt("SCREENID"), result.getString("FILMDATE") );
             showings.add( specificShow );
        } 
        }catch( Exception e ){
             JOptionPane.showMessageDialog( null, "Problem Connecting" );
        } 
        
        //For each showing retrieved from the database, add button to book film
        for( Showing showing : showings ){
            timeButton = new JButton( "<html><body style='width:75px'>" + showing.getFilmDate() + " \n" + showing.getFilmTime() + "\n Book Tickets" );
            bookTicket.add( timeButton );
            timeButton.setBounds(xPos, yPos, 200, 200);
            bGPanel.add( timeButton );
            int indexOf = showings.indexOf(showing);
            timeButton.setActionCommand( "" + indexOf );
            xPos = xPos + 250;  
            if( xPos > 710 ){
                xPos = 10;
                yPos = yPos + 250;
            }
        }

        //For each showing button, add listener to the button to allow user to book film
        for(int i = 0;i<bookTicket.size(); i++){ 
            bookTicket.get(i).addActionListener(new ActionListener(){
               public void actionPerformed( ActionEvent ae ){
                   int index = Integer.parseInt( ae.getActionCommand() );
                    if( Login.loggedIn == true){ 
                       ChooseSeatsFrame chooseSeats = new ChooseSeatsFrame( filmName, showings.get(index).getFilmTime(), showings.get(index).getScreen(), showings.get(index).getFilmDate() , showings.get(index).getShowID() );
                    }else{
                        JOptionPane.showMessageDialog( null,"Please Log in to book tickets" );
                    }
                }              
                });
        }
        
        //Set bounds to swing elements
        poster.setBounds(10,25,130,200);
        back.setBounds(600,10,100,20);
        details.setBounds(150,25,560,200);
        vreviews.setBounds(450,200,200,20);
        wreviews.setBounds(200,200,200,20);
        
        details.setFont( new Font( details.getFont().getFontName(), Font.PLAIN, 20) );
        bGPanel.add( wreviews );
        bGPanel.add( vreviews );
        bGPanel.add( poster );
        bGPanel.add( details );
        bGPanel.add( back );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setVisible(true);
        setSize(750,750);
        setLocationRelativeTo(null);
        
        clickVReviews();
        clickWReviews();
        clickBack();
    }
    
    //Allows user to view reviews
    public void clickVReviews(){
        vreviews.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    ViewReviewsFrame frame = new ViewReviewsFrame(filmName);
                }
        });
    }
    
    //Allows user to write reviews, for logged in
    public void clickWReviews(){
        wreviews.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                if( Login.loggedIn == true){
                    WriteReviewFrame frame = new WriteReviewFrame( filmName );
                }else{
                        JOptionPane.showMessageDialog( null, "Please log in to write a review" );
                    }
                }
        });   
    }   
    
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }   
}
