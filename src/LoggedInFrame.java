
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 * LoggedInFrame Class
 * Home/Main frame for users once logged in.
 * @Author: Niral Patel
 * @version 1.0
 */

public class LoggedInFrame extends JFrame{
  //Setup swing elements
  ImageIcon image = new ImageIcon( "log out.jpg" );
  ImageIcon image1 = new ImageIcon( "exit.jpg" );
  ImageIcon image2 = new ImageIcon( "my bookings.jpg" );
  LinkedList<String> filmImagesList = new LinkedList();
  LinkedList<String> filmnameList = new LinkedList();
  ArrayList<JButton> buttons = new ArrayList();
  ArrayList<String> filmHyperlinks = new ArrayList();
  ArrayList<Film> films = new ArrayList(); 
  JButton blogOut = new JButton( image );
  JButton exit = new JButton( image1 );
  JButton bBookings = new JButton( image2 );
  JLabel bNewsletter;
  String newsletter = "";
  JLabel loggedInUserName = new JLabel();
  static Image backGround;
  BrowserButton panel;
    
    
    LoggedInFrame( String userName ){
        super( "Home" );
        try{
            //Attempt to retrieve list of films showing
            filmImagesList = DBConnection.getLLQuery( "select filmposter from film order by filmname", DBConnection.connection );
            filmnameList = DBConnection.getLLQuery( "select filmname from film order by filmname", DBConnection.connection );
            newsletter = DBConnection.getQuery( "select newstext from newsletter", DBConnection.connection );
        }catch(Exception e){
            JOptionPane.showMessageDialog( null, "Error with database" );
        }
        
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        loggedInUserName = new JLabel( "Currently logged in: " + Login.loggedInUserName );
        bNewsletter = new JLabel( "<html><body style='width:500px'>" + newsletter );
        bNewsletter.setBackground(Color.white);
        bNewsletter.setOpaque(true);
        
        int xPos = 50;
        int yPos = 50;
        
        //Display the retrieved posters for the films
        for( String link : filmImagesList ){
            Image filmImage = ReadImage.getImage( link );
            Image newFilmImage = filmImage.getScaledInstance(250, 400, java.awt.Image.SCALE_SMOOTH);
            panel = new BrowserButton( newFilmImage );
            buttons.add( panel );
            filmHyperlinks.add(link);
            panel.setBounds(xPos, yPos, 250, 400);
            bGPanel.add(panel);
            xPos = xPos + 300;
            if( xPos >= 1200 ){
                xPos = 50;
                yPos = yPos + 450;
            }
          }
          
          for( String name : filmnameList ){
              Film film = new Film();
              film.retrieve( name );
              films.add( film );
          }
          
            for(int i=0; i< buttons.size(); i++){
            buttons.get(i).setActionCommand( filmnameList.get(i) );
            buttons.get(i).addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                String filmname = e.getActionCommand();
                for( Film film : films ){
                    if ( film.getFilmName().equals( filmname ) ){
                        FilmDetailsFrame filmInfo = new FilmDetailsFrame( film.getFilmName(), film.getPoster() , film.getSynopsis() );
                    }
                }
            }
            });
        }

        //Set bounds for swing elements
        exit.setBounds(1770,10,100,20);
        loggedInUserName.setBounds(50,10,250,20);
        blogOut.setBounds(1620,10,100,20);
        bBookings.setBounds(500,10,150,20);
        bNewsletter.setBounds(1270,50,600,900);

        bGPanel.add( exit );
        bGPanel.add(loggedInUserName);
        bGPanel.add(blogOut);
        bGPanel.add(bBookings);
        bGPanel.add(bNewsletter);
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0,0,screenSize.width, screenSize.height);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        clickLogout();
        clickBookings();
        clickExit();
    }
    
    //Log user out from system, update flags and reset object states
    public void clickLogout(){
            blogOut.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                   Login.loggedIn = false;
                   Login.loggedInUserName = "";
                   ImageInJFrame.mainFrame.setVisible(true);
                     filmImagesList = new LinkedList();
                     filmnameList = new LinkedList();
                     buttons = new ArrayList();
                     filmHyperlinks = new ArrayList();
                     films = new ArrayList();
                     dispose();
                }
                });
    }
    
    //Allow a user to view his bookings, opens booking frame
    public void clickBookings(){
            bBookings.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    MyBookingsFrame bookingsFrame = new MyBookingsFrame( Login.loggedInUserName );
                }
                });
    }
    
    public void clickExit(){
        exit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    System.exit(0);
                }
        });
    } 
    
    }

