import java.sql.ResultSet; 
import java.sql.Statement; 
import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 * Seats class
 * Stores information about seats already booked for a specific screen
 * @version 1.0
 */
public class Seats {

    private int screen; 
    private LinkedList<Integer> seatNumber; 

    /**
    * Default constructor
    */
    Seats(){} 

    /**
     * Constructor
     * @param screen The ID of the screen
     * @param seatNumber A linked list of seat numbers already booked
     */
    Seats( int screen, LinkedList<Integer> seatNumber ){
        this.screen = screen; 
        this.seatNumber = seatNumber; 
    } 

    // Getters for this class:

    /**
     * @return The Screen ID
     */
    public int getScreen(){
        return this.screen; 
    } 

    /**
     * @return A linked List of seat numbers taken
     */
    public LinkedList<Integer> getSeatNumber(){
        return this.seatNumber; 
    } 

    // Setters for this class:

    /**
     * Sets the screen ID
     * @param screen The screen ID
     */
    public void setScreen( int screen ){
        this.screen = screen; 
    } 

    /**
     * Sets the list of seat numbers booked
     * @param seatNumber A linked list of seat numbers
     */
    public void setSeatNumber( LinkedList<Integer> seatNumber ){
        this.seatNumber = seatNumber; 
    } 

    // Database Functionality for this class:

    /**
     * Checks if a particular seat is booked
     * @param screen The ID number of the screen
     * @param seatNumber The number of the seat
     */
    public boolean isBooked( int screen, int seatNumber ){
        boolean booked = false; 
        String result = ""; 
        
        try{
            result = DBConnection.getQuery( "select seatnumber from seats where screen = " + screen , DBConnection.connection ); 
        }catch( Exception e){ 
            JOptionPane.showMessageDialog( null, "Error" );
        } 
        
        if( result.equals(String.valueOf(seatNumber) ) ){
            booked = true; 
        } return booked; 

    } 

    /**
     * Loads the object from the database given the screen ID number
     * @param screen The screen ID
     */
    public void retrieve( int screen ){
        try{
            this.screen = screen; seatNumber = DBConnection.getLLIntQuery("select seatNumber from seats where screen = " + this.screen, DBConnection.connection); 
        }catch( Exception e ){ } 
    } 

    /**
     * Saves the object to the database
     */
    public void save(){
        for( int seat: seatNumber ){
            try{ 
            DBConnection.addQuery( "inset into seats values(" + screen +","+ seat +")" , DBConnection.connection ); 
            }catch( Exception e ){ 
                JOptionPane.showMessageDialog( null, "Error" );
            } 
        } 
    } 
}