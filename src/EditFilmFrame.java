
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/*
 * This is the EditFilmFrame class 
 */
public class EditFilmFrame extends JFrame{
    ImageIcon image = new ImageIcon( "back.jpg" );
    ImageIcon image1 = new ImageIcon( "edit film synopsis.jpg" );
    ImageIcon image2 = new ImageIcon( "edit film review.jpg" );
    JButton back = new JButton( image );
    JLabel filmID = new JLabel( "Film Name" );
    JTextField filmName = new JTextField();
    JButton bEditDetails = new JButton( image1 );
    JButton bEditReview = new JButton( image2 );
    
    EditFilmFrame(){
        super( "Choose Film" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);

        filmID.setBounds(115,75,100,20);
        filmName.setBounds(195,75,150,20);
        bEditDetails.setBounds(50,150,200,20);
        bEditReview.setBounds(300,150,200,20);
        back.setBounds(400,10,100,20);
        bGPanel.add(back);
        bGPanel.add( filmID );
        bGPanel.add( filmName );
        bGPanel.add( bEditDetails );
        bGPanel.add( bEditReview );
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(600,250);
        setLocationRelativeTo(null);
        setVisible(true);
        clickEditDetails();
        clickEditReview();
        clickBack();
    }
    
    public void clickEditDetails(){
        bEditDetails.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    String searchResult = "";
                    String filmname = filmName.getText();
                    if( filmname.equals("") ){
                        JOptionPane.showMessageDialog( null, "Please enter a film" );
                    }else{
                    try{
                    searchResult = DBConnection.getQuery( "select filmname from film where filmname = '" + filmname + "'", DBConnection.connection );
                    }catch(Exception e ){
                        JOptionPane.showMessageDialog( null, "Database Error" );
                    }
                    if( filmname.equals( searchResult ) ){
                        EditFilmDetailsFrame details = new EditFilmDetailsFrame( filmname );
                    }else{
                        JOptionPane.showMessageDialog( null, "Film not in database" );
                    }
                    }
                }
                });   
    }
    
    public void clickEditReview(){
        bEditReview.addActionListener( new ActionListener(){
                public void actionPerformed( ActionEvent ae ){
                    String searchResult = "";
                    String filmname = filmName.getText();
                     if( filmname.equals("") ){
                        JOptionPane.showMessageDialog( null, "Please enter a film" );
                    }else{
                    try{
                    searchResult = DBConnection.getQuery( "select filmname from film where filmname = '" + filmname + "'", DBConnection.connection );
                    }catch(Exception e ){
                        JOptionPane.showMessageDialog( null, "Database Error" );
                    }
                    if(filmname.equals( searchResult )){
                    EditFilmReviewsFrame details = new EditFilmReviewsFrame( filmname );
                    }else{
                        JOptionPane.showMessageDialog( null, "Film not in database" );
                    }
                     }
                }
                });
    }
    
    public void clickBack(){
        back.addActionListener(new ActionListener(){
                public void actionPerformed( ActionEvent e ){
                    dispose();
                }
        });   
    }  
}
