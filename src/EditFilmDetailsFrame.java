
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/*
 * This is the EditFIlmDetailsFrame which creates
 * a GUI frame for tha admin to interact with to change
 * a film synopsis
 * 
 * @author  Niral Patel
 * @version v1.0
 */
public class EditFilmDetailsFrame extends JFrame{
    ImageIcon image = new ImageIcon( "save.jpg" );
    ImageIcon image1 = new ImageIcon( "cancel.jpg" );
    JTextArea synopsis;
    JLabel title = new JLabel( "Please enter new film synopsis" );
    JButton save = new JButton( image );
    JButton cancel = new JButton( image1 );
    Film film = new Film();
    
    /*
     * EditFilmDetailsFrame constructor
     * 
     * creates the GUI frame
     * 
     * @param   filmname    the name of the film to edit
     */
    EditFilmDetailsFrame( String filmname ){
        //various varaibles and image files for the GUI
        super( "Edit Synopsis" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        film.retrieve( filmname );
        
        //adding and positioning the components
        synopsis = new JTextArea( film.getSynopsis() );
        synopsis.setLineWrap(true);
        synopsis.setBounds(50,75,650,300);
        save.setBounds(50,400,100,20);
        cancel.setBounds(600,400,100,20);
        title.setBounds(200,10,300,20);
        bGPanel.add(title);
        bGPanel.add(synopsis);
        bGPanel.add(save);
        bGPanel.add(cancel);
        
        //adding scrollbar to the frame
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(850,500);
        setLocationRelativeTo(null);
        setVisible(true);
        clickSave();
        clickCancel();
    }
    
    /*
     * clickSave method 
     * 
     * adds the new synopsis to the database,
     * replacing the old one when save is clicked
     * 
     */
    public void clickSave(){
        save.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    film.setSynopsis( synopsis.getText() );
                    film.save();
                    JOptionPane.showMessageDialog( null, "The changes have been saved" );
                    dispose();
                }
        });   
    } 
    
    /*
     * clickCancel method
     * 
     * diposes the frame the cancel button is pressed
     */
    public void clickCancel(){
         cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
         });
     }
}
