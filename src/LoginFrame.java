
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * LoginFrame Class
 * Frame for users/customers to login.
 * @author Niral Patel
 */
 
public class LoginFrame extends JFrame{

     JLabel welcome = new JLabel( "Please enter your details" );
     JPanel panel = new JPanel();
     JButton blogin = new JButton("Login");
     JButton cancel = new JButton( "Cancel");
     JButton bcreate = new JButton("Create Account");
      JTextField txuser = new JTextField(15);
      JPasswordField pass = new JPasswordField(15);
      JLabel labelUser = new JLabel( "Username" );
      JLabel labelPassword = new JLabel( "Password" );

     LoginFrame(){
         super("Login");
         setSize(440,200);
         setLocationRelativeTo(null);  
         panel.setLayout(null);

         cancel.setBounds(260,120,150,20);
         welcome.setBounds(100,5,300,60);
         txuser.setBounds(100,50,150,20);
         pass.setBounds(100,85,150,20);
         blogin.setBounds(100,120,150,20);
         bcreate.setBounds(260,50,150,55);
         labelUser.setBounds(25,50,100,20);
         labelPassword.setBounds(25,85,100,20);
       
         panel.add(cancel);
         panel.add( welcome );
         panel.add( blogin );
         panel.add( bcreate );
         panel.add( txuser );
         panel.add( pass );
         panel.add( labelUser );
         panel.add( labelPassword );

         getContentPane().add(panel);
         setVisible(true);
         actionLogin();
         createAccount();
         clickCancel();
     }
     
     //Attempt to login account using input user & pass, checks if valid and informs user if error
     public void actionLogin(){
            
            blogin.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    String userName = txuser.getText();
                    userName = userName.replace("\"", "");
                    String userPassword = pass.getText();
                    userPassword = userPassword.replace("\"", "");
                    boolean inDB = Login.login(userName, userPassword);
                    if( inDB == true ){
                        JOptionPane.showMessageDialog( null, "You are now logged in" );
                        LoggedInFrame frame = new LoggedInFrame(userName);
                        ImageInJFrame.mainFrame.setVisible(false);
                        dispose();
                    }
                    else if( inDB == false ){
                        JOptionPane.showMessageDialog( null, "Incorrect username or password" );
                        txuser.requestFocus();
                    }
                         
                }
            });
    }
     
     //If createAccount button clicked, open panel so they can create an account
     public void createAccount(){
         bcreate.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    NewCreateAccountFrame createFrame = new NewCreateAccountFrame();
                }
            });
     }  
     
     public void clickCancel(){
         cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
         });
     }
 }

