
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 * NewsletterFrame class
 * Frame for creating new newsletters.
 * @version 1.0
 * @author Niral Patel
 */

public class NewsletterFrame extends JFrame{
    ImageIcon image = new ImageIcon( "create newsletter.jpg" );
    ImageIcon image1 = new ImageIcon( "edit current newsletter.jpg" );
    ImageIcon image2 = new ImageIcon( "cancel.jpg" );
    JButton create = new JButton( image );
    JButton edit = new JButton( image1 );
    JButton cancel = new JButton( image2 );

    NewsletterFrame(){
        super( "Newsletter Options" );
        Browser bGPanel = new Browser( ImageInJFrame.backGround );
        add(bGPanel);
        
        create.setBounds(50,20,150,20);
        edit.setBounds(500,20,200,20);
        cancel.setBounds(600,370,100,20);
        
        bGPanel.add(cancel); //Add cancel button to panel
        bGPanel.add(create); //Add create newsletter button to panel
        bGPanel.add(edit);   //Add edit newsletter button to panel
        
        JScrollPane pane = new JScrollPane(bGPanel);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(pane);
        
        setSize(800,450);
        setLocationRelativeTo(null);
        setVisible(true);
        clickCreate();
        clickEdit();
        clickCancel();
    }
    
    //Button to open new CreateNewsletter frame
    public void clickCreate(){
        create.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    CreateNewsletterFrame frame = new CreateNewsletterFrame();
                    dispose();
                }
        });
    }
    
     //Button to open new EditNewsletter frame
    public void clickEdit(){
        edit.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    EditNewsletterFrame frame = new EditNewsletterFrame();
                    dispose();
                }
        });            
    }
    
    public void clickCancel(){
        cancel.addActionListener( new ActionListener() {
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
                });
        
    }
}
