import java.sql.Date;
import java.sql.ResultSet; 
import java.sql.Statement;

/**
 * Showing class
 * Stores information relating to showings for a film
 * @version 1.0
 */
public class Showing {
    
    private int showID;
    private String filmName;
    private int filmTime;
    private int screen;
    private String filmDate;
    
    /**
    * Default constructor
    */
    Showing(){}
    
    /**
     * Constructor
     * @param showID The ID number for the Showing
     * @param filmName The name of the film being shown
     * @param filmTime The time of the Showing
     */
    Showing( int showID, String filmName, int filmTime, int screen, String filmDate ){
        this.showID = showID;
        this.filmName = filmName;
        this.filmTime = filmTime;
        this.screen = screen;
        this.filmDate = filmDate;
    }
    
    // Getters for this class:
    
    /**
     * @return The ID number of the Showing
     */
    public int getShowID(){
        return this.showID;
    }
    
    /**
     * @return The name of the film being shown
     */
    public String getFilmName(){
        return this.filmName;
    }
    
    /**
     * @return The time of the Showing
     */
    public int getFilmTime(){
        return this.filmTime;
    }
    
    /**
     * @return The screen for this Showing
     */
    public int getScreen(){
        return this.screen;
    }
    
    /**
     * @return The date for this Showing
     */
    public String getFilmDate(){
        return this.filmDate;
    }
    
    // Setters for this class:
    
    /**
     * Sets the ID number for the Showing
     * @param showID The ID number
     */
    public void setShowID( int showID ){
        this.showID = showID;
    }
    
    /**
     * Sets the name of the film being shown
     * @param filmName The name of the film
     */
    public void setFilmName( String filmName ){
        this.filmName = filmName;
    }
    
    /**
     * Sets the time of the Showing
     * @param filmTime The time of the Showing
     */
    public void setFilmTime( int filmTime ){
        this.filmTime = filmTime;
    }
    
    /**
     * Sets the screen for this Showing
     * @param screenNumber The screen of the Showing
     */
    public void setScreen( int screenNumber ){
        this.screen = screenNumber;
    }
    
    /**
     * Sets the date for this Showing
     * @param filmDate The date of the Showing
     */
    public void setFilmDate( String filmDate ){
        this.filmDate = filmDate;
    }
    
    // Database functionality for this class:
    
    /**
     * Loads information from the database as long as the Showing ID is set
     * @param showID The id of the Showing
     */
    public void retrieveShowings( int showID ){ 

        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select showid, filmname, filmtime, screenid, filmdate from showing where showid = " + showID ); 

            while( result.next() ){ 
                this.showID = result.getInt( "SHOWID" ); 
                this.filmName = result.getString( "FILMNAME" ); 
                this.filmTime = result.getInt( "FILMTIME" ); 
                this.screen = result.getInt( "SCREENID" ); 
                this.filmDate = result.getString( "FILMDATE" ); 
            } 
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving showing" );
        } 
    } 
    
    /**
     * Retrieves information from the database as long as the filmname is set
     * @param filmname The title of the film
     */
    public void retrieveShowingsForFilm( String filmname ){ 

        try{ 
            Statement statement = DBConnection.connection.createStatement(); 
            ResultSet result = statement.executeQuery( "select showid, filmtime, screen, filmdate from showing where filmname = '" + filmname + "'" ); 

            while( result.next() ){ 
                this.showID = result.getInt( "SHOWID" ); 
                this.filmTime = result.getInt( "FILMTIME" ); 
                this.screen = result.getInt( "SCREEN" ); 
                this.filmDate = result.getString( "FILMDATE" ); 
            } 
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, "Error retrieving showing" );
        } 
    } 

    /**
     * Saves the Showing to the database
     */
    public void save(){
        try{ 
            DBConnection.addQuery( "update showings set bookingid = " + showID + ", filmname = '" + filmName + "', filmtime = " + filmTime + " where showID = " + showID , DBConnection.connection ); 
        }catch( Exception e ){ 
            JOptionPane.showMessageDialog( null, "Error saving showing" );
        } 
    }     
}
