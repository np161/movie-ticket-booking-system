
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/*
 * This is the AdminLoginFrame class used to create a GUI panel for the  
 * administrator to interact with for logging into the system.
 * 
 * Author: Niral Patel
 * Version: v1.0
 * 
 */

public class AdminLoginFrame extends JFrame{
    /*
     * Various image files used for the buttons, instead of 
     * standard swing buttons.
     */
     JLabel welcome = new JLabel( "Please enter your details" );
     JPanel panel = new JPanel();
     JButton blogin = new JButton( "Login" );
     JButton cancel = new JButton( "Cancel" );
     JTextField txuser = new JTextField(15);
     JPasswordField pass = new JPasswordField(15);
     JLabel labelUser = new JLabel( "Username" );
     JLabel labelPassword = new JLabel( "Password" );

     AdminLoginFrame(){
         //Setting up the frame and the positioning of the various buttons
         super("Admin Login");
         setSize(440,200);
         setLocationRelativeTo(null);  
         panel.setLayout (null);

         welcome.setBounds(100,5,300,60);
         txuser.setBounds(100,50,150,20);
         pass.setBounds(100,85,150,20);
         blogin.setBounds(100,120,150,20);
         cancel.setBounds(260,120,150,20);
         labelUser.setBounds(25,50,100,20);
         labelPassword.setBounds(25,85,100,20);
       

         panel.add( welcome );
         panel.add( blogin );
         panel.add(cancel);
         panel.add( txuser );
         panel.add( pass );
         panel.add( labelUser );
         panel.add( labelPassword );

         getContentPane().add(panel);
         setVisible(true);
         //Various methods for when buttons are pressed, to 
         //attempt to log the Admin into the system.
         actionLogin();
         clickCancel();
     }
     
     /*
      * actionLogin method
      * 
      * when the blogin button is clicked, the method 
      * collects the information from the text fields
      * to call back-end methods to verify the user 
      * credentials
      * 
      */
     public void actionLogin(){
            blogin.addActionListener( new ActionListener() {
                @Override
                public void actionPerformed( ActionEvent ae ){
                    String userName = txuser.getText();
                    userName = userName.replace("\"", "");
                    String userPassword = pass.getText();
                    userPassword = userPassword.replace("\"", "");
                    boolean inDB = Login.adminLogin(userName, userPassword);
                    if( inDB == true ){
                        JOptionPane.showMessageDialog( null, "You are now logged in" );
                        AdminFrame frame = new AdminFrame(userName);
                        ImageInJFrame.mainFrame.setVisible(false);
                        dispose();
                    }
                    else if( inDB == false ){
                        JOptionPane.showMessageDialog( null, "Incorrect username or password" );
                        txuser.requestFocus();
                    }
                         
                }
            });
    }
     
     /*
      * clickCancel method
      * 
      * when the cancel button is clicked the method 
      * disposes the current [@AdminLoginFrame].
      * 
      */
     public void clickCancel(){
         cancel.addActionListener( new ActionListener() {
                @Override
                public void actionPerformed( ActionEvent ae ){
                    dispose();
                }
         });
     }
}
     